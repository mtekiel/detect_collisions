<?php

// POINT-POINT Function checks collisions between two objects 
function pointPoint( $x1, $y1, $x2, $y2) {
    if ($x1 == $x2 && $y1 == $y2) {
        echo "<br><b>Collision detected between objects:</b><br>
            Point A( ".$x1." , ".$y1. " )<br>
            Point B( ".$x2." , ".$y2." )<br>";
        return true;
    }
    echo "<br><b>No collision detected between objects:</b><br>
        Point A( " . $x1 . " , " . $y1 . " )<br>
        Point B( " . $x2 . " , ". $y2 . " )<br> ";
    return false;
}

// POINT-CIRCLE
function pointCircle($px, $py, $circleX, $circleY, $radius) {

	// Get distance between the point and circle's center using the Pythagorean Theorem
	$distX = $px - $circleX;
	$distY = $py - $circleY;
	$distance = sqrt( ($distX*$distX) + ($distY*$distY) );

	// If the distance is less than the circle's radius the point is inside
	if ($distance <= $radius) {
		echo "<br><b>Collision detected between objects:</b><br>
            Point A( " . $px . " , " . $py . " )<br>
            Circle (center( " . $circleX . " , " . $circleY . " ) , radius = ".$radius." )<br>";
		return true;
	}
	echo "<br><b>No collision detected between objects:</b><br>
        Point A( " . $px . " , " . $py . " )<br>
        Circle (center( " . $circleX . " , " . $circleY . " ) , radius = " . $radius . " )<br>";
	return false;
}

//CIRCLE-CIRCLE
function circleCircle( $circle1X, $circle1Y, $radius1, $circle2X, $circle2Y, $radius2) {

    // Get distance between the circle's centers using the Pythagorean Theorem to compute the distance
    $distX = $circle1X - $circle2X;
    $distY = $circle1Y - $circle2Y;
    $distance = sqrt( ($distX * $distX) + ($distY * $distY) );

    // If the distance is less than the sum of the circle's radii, the circles are touching.
    if ($distance <= $radius1 + $radius2) {
        echo "<br><b>Collision detected between objects:</b><br>
            Circle (center( " . $circle1X . " , " . $circle1Y . " ) , radius = " . $radius1 . ")<br>
            Circle (center( " . $circle2X . " , " . $circle2Y . " ) , radius = " . $radius2 . ")</br><br>";
        return true;
    }
    echo "<br><b>No collision detected between objects:</b><br>
        Cirlce (center( " . $circle1X . " , " . $circle1Y . " ) , radius = " . $radius1 . ")<br>
        Circle (center( " . $circle2X . " , " . $circle2Y . " ) , radius = " . $radius2 . ")<br>";
    return false;
}


// POINT-RECTANGLE
function pointRect( $px,  $py,  $leftBottomCornerX,  $leftBottomCornerY,  $rectWidth,  $rectHeight) {

    // Check if the point is inside the rectangle's bounds
    if ($px >= $leftBottomCornerX &&        // right of the left edge AND
        $px <= $leftBottomCornerX + $rectWidth &&   // left of the right edge AND
        $py >= $leftBottomCornerY &&        // above the bottom AND
        $py <= $leftBottomCornerY + $rectHeight) {   // below the top
        echo "<br><b>Collision detected between objects:</b><br>
            Point A(" . $px . "," . $py . ")<br> 
            Rectangle (Left Bottom Corner( " . $leftBottomCornerX . " , " . $leftBottomCornerY . " ) , width = ".$rectWidth." ,  height = " . $rectHeight . " )<br>";    
        return true;
    }
    echo "<br><b>No collision detected between objects:</b><br>
        Point A( " . $px . " , " . $py . " )<br>
        Rectangle (Left Bottom Corner( " . $leftBottomCornerX . " , " . $leftBottomCornerY . " ) , width = " . $rectWidth . " , height = " . $rectHeight . ")<br>"; 
    return false;
}

// RECTANGLE-RECTANGLE
//We start drawing rectangles from Left Bottom Corner - LBCorner

function rectRect( $rect1LBCornerX,  $rect1LBCornerY, $rect1Width, $rect1Height, $rect2LBCornerX, $rect2LBCornerY, $rect2Width, $rect2Height) {

  // Check if the sides of one rectangle touching the other

    if ($rect1LBCornerX + $rect1Width >= $rect2LBCornerX &&    // rect1 right edge past rect2 left
        $rect1LBCornerX <= $rect2LBCornerX + $rect2Width &&    // rect1 left edge past rect2 right
        $rect1LBCornerY + $rect1Height >= $rect2LBCornerY &&    // rect1 top edge past rect2 bottom
        $rect1LBCornerY <= $rect2LBCornerY + $rect2Height) {    // rect1 bottom edge past rect2 top
        
        echo "<br><b>Collision detected between objects:</b><br>
            Rectangle( Left Bottom Corner( " . $rect1LBCornerX . " , " . $rect1LBCornerY . " ) , width = " . $rect1Width . " , height = " . $rect1Height . " )<br>
            Rectangle( Left Bottom Corner ( " . $rect2LBCornerX . " , " . $rect2LBCornerY . " ) , width = " . $rect2Width . " , height = " . $rect2Height . " )<br>";  
        return true;
    }
    echo
    "<br><b>No collision detected between objects:</b><br>
        Rectangle( Left Bottom Corner( " . $rect1LBCornerX . " , " . $rect1LBCornerY . " ) , width = " . $rect1Width . " , height = " . $rect1Height . " )<br>
        Rectangle( Left Bottom Corner ( " . $rect2LBCornerX . " , " . $rect2LBCornerY . " ) , width = " . $rect2Width . " , height = " . $rect2Height . " )<br>";  
    return false;
}

// CIRCLE/RECTANGLE
function circleRect($circleX, $circleY, $radius, $rectLBCornerX, $rectLBCornerY, $rectWidth, $rectHeight) {

    // Temporary variables to set edges for testing
    $testX = $circleX;
    $testY = $circleY;

    // We check which edge is closest
    if ($circleX < $rectLBCornerX)         $testX = $rectLBCornerX;      // test left edge
    else if ($circleX > $rectLBCornerX+$rectWidth) $testX = $rectLBCornerX+$rectWidth;   // right edge
    if ($circleY < $rectLBCornerY)         $testY = $rectLBCornerY;      // top edge
    else if ($circleY > $rectLBCornerY+$rectHeight) $testY = $rectLBCornerY+$rectHeight;   // bottom edge

    // Get distance from closest edges
    $distX = $circleX-$testX;
    $distY = $circleY-$testY;
    $distance = sqrt( ($distX*$distX) + ($distY*$distY) );

    // If the distance is less or equal than the radius it is collision.
    if ($distance <= $radius) {
        echo "<br><b>Collision detected between objects:</b><br>
            Circle (center( " . $circleX . " , " . $circleY . " ) , radius = " . $radius . " )<br>
            Rectangle (Left Bottom Corner( " . $rectLBCornerX . " , " . $rectLBCornerY . " ) , width = " . $rectWidth . " , height = " . $rectHeight . ")<br>   ";
        return true;
    }
    echo "<br><b>No collision detected between objects:</b><br>
        Circle (center( " . $circleX . " , " . $circleY . " ) , radius = " . $radius . " )<br>
        Rectangle (Left Bottom Corner( " . $rectLBCornerX . " , " . $rectLBCornerY . " ) , width = " . $rectWidth . " , height = " . $rectHeight . ")<br>   ";
    return false;
    }

// TRIANGLE/POINT
function trianglePoint($triangleX1, $triangleY1, $triangleX2, $triangleY2, $triangleX3, $triangleY3, $px, $py) {

	// To get the area of the triangle we use Heron's formula
	$areaOrig = abs( ($triangleX2-$triangleX1)*($triangleY3-$triangleY1) - ($triangleX3-$triangleX1)*($triangleY2-$triangleY1) );

	// Get the area of 3 triangles made between the point and the vertices of the triangle
	$area1 = abs( ($triangleX1-$px)*($triangleY2-$py) - ($triangleX2-$px)*($triangleY1-$py) );
	$area2 = abs( ($triangleX2-$px)*($triangleY3-$py) - ($triangleX3-$px)*($triangleY2-$py) );
	$area3 = abs( ($triangleX3-$px)*($triangleY1-$py) - ($triangleX1-$px)*($triangleY3-$py) );

	// If the sum of the three areas equals the original, we are inside the triangle
	if ($area1 + $area2 + $area3 == $areaOrig) {
        echo "<br><b>Collision detected between objects:</b><br>
            Triangle ( V1( " . $triangleX1 . " , " . $triangleY1 . " ) , V2( " . $triangleX2 . " , " . $triangleY2 . " ), V3( " . $triangleX3 . " , " . $triangleY3 . " ))<br>
            Point A( " . $px . " , " . $py . " )<br>";
		return true;
	}
    echo "<br><b>No collision detected between objects:</b><br>
        Triangle ( V1( " . $triangleX1 . " , " . $triangleY1 . " ) , V2( " . $triangleX2 . " , " . $triangleY2 . " ), V3( " . $triangleX3 . " , " . $triangleY3 . " ))<br>
        Point A( " . $px . " , " . $py . " )<br>";
	return false;
}


//Fuction creating part of data form for point
function pointForm($num = ""){

    $form = '<b><span>Point' . $num . '</span></b><br>
        <label>Point' . $num . ' X coordinate</label>
        <input type="text" name="px' . $num . '"><br>
        <label>Point' . $num . ' Y coordinate</label>
        <input type="text" name="py' . $num . '"><br><br>';

    return $form;
}


//Fuction creating part of data form for circle
function circleForm($num=""){

    $form = '<b><span>Circle' . $num . '</span></b><br>
        <label>Centre X coordinate</label>
        <input type="text" name="circle' . $num . 'X"><br>
        <label>Centre Y coordinate</label>
        <input type="text" name="circle' . $num . 'Y"><br>
        <label>Circle1 radius length</label>
        <input type="text" name="radius' . $num . '"><br><br>';

    return $form;
}

//Fuction creating part of data form for rectangle
function rectangleForm($num = ""){
    
    $form = '<b><span>Rectangle' . $num . '</span></b><br>
        <label>Left Bottom Corner X</label>
        <input type="text" name="rect' . $num . 'LBCornerX"><br>
        <label>Left Bottom Corner Y</label>
        <input type="text" name="rect' . $num . 'LBCornerY"><br>
        <label>Rectangle' . $num . ' Width ___ </label>
        <input type="text" name="rect' . $num . 'Width"><br>
        <label>Rectangle' . $num . ' Height __</label>
        <input type="text" name="rect' . $num . 'Height"><br><br>';

    return $form;
}


