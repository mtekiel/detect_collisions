<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <title>Collision Detection</title>
</head>

<body>
    <header>        
        <nav class="navbar navbar-expand-md navbar-dark bg-dark navbar-default navbar-static-top">
            <a class="navbar-brand" href="#">Collision Detector</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <div class="dropdown show">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="link" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            2D PAIRS
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="index.php?shape1=point&shape2=point">Point - Point</a>
                            <a class="dropdown-item" href="index.php?shape1=circle&shape2=point">Circle - Point</a>
                            <a class="dropdown-item" href="index.php?shape1=circle&shape2=circle">Circle - Circle</a>
                            <a class="dropdown-item" href="index.php?shape1=circle&shape2=rectangle">Circle - Rectangle</a>
                            <a class="dropdown-item" href="index.php?shape1=rectangle&shape2=point">Rectangle -Point</a>
                            <a class="dropdown-item" href="index.php?shape1=rectangle&shape2=rectangle">Rectangle - Rectangle</a>
                            <a class="dropdown-item" href="index.php?shape1=triangle&shape2=point">Triangle - Point</a>
                        </div>
                    </div>
                    &nbsp &nbsp
                    <div class="dropdown show">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="link" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            MULTI OBJECTS
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="multiObjects.php?shape1=circle&shape2=circle&shape3=circle">3 Circles</a>
                            <a class="dropdown-item" href="multiObjects.php?shape1=rectangle&shape2=rectangle&shape3=rectangle">3 Rectangles</a>
                            <a class="dropdown-item" href="multiObjects.php?shape1=rectangle&shape2=circle&shape3=circle">1 Rectangle and 2 Circles </a>
                            <a class="dropdown-item" href="multiObjects.php?shape1=circle&shape2=rectangle&shape3=rectangle">1 Circle and 2 Rectangles </a>
                        </div>
                    </div>

                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>