DETECT COLLISIONS PHP PROJECT 

1. I started the task with creating the application structure.
   Folders for css, js and phpunittest.  File functions.php containing all functions used in task. 
   The menu bar that will be include to many pages was placed in header.php.

2. Next step was creating bundle of functions detecting collisions between various objects.

3. Parallel to the creation of the function, I was creating a suitable simple bootstrap 4 front end for data entry.   
   In order not to repeat the code and simplify building forms for testing data, I wrote few functions - pointForm(), circleForm(), rectangleForm().

4. To test collision detection functions, I created appropriate test scenarios using PHPUnit.

5. Having already the collision detection functions on pairs of basic objects, I use them to detect collisions of more objects.

6. Project Repository is under link https://bitbucket.org/mtekiel/detect_collisions/src/dev/
