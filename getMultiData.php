<?php
require_once('functions.php');
require_once('header.php');
?>
<main>
    <div class="content-wrapper">
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-3"><a href="multiObjects.php" class="btn btn-warning">Back</a></div>
                    <div class="col-6">
                        <?php
                        $shape1 = $_POST['shape1'];
                        $shape2 = $_POST['shape2'];
                        $shape3 = $_POST['shape3'];
                       
                        if (!$shape1 && !$shape2 && !$shape3 ) {
                            echo "Input shape";
                        }

                        //Repetitive part of form
                        echo '<form action="checkMultiCollisions.php" method="POST">
                            <b><label>Object1</label>
                            <input type="text" name="shape1" value="' . $shape1 . '">&nbsp&nbsp                                     
                            <label>Object2</label>
                            <input type="text" name="shape2" value="' . $shape2 . '"><br><br>
                            <label>Object3</label>
                            <input type="text" name="shape3" value="' . $shape3 . '"><br><br></b>';

                        //Check what kind of collisions we just have and load appropriate form for data
                        if ($shape1 == "circle" && $shape2 == "circle" && $shape3 == "circle") {
                            echo circleForm(1);
                            echo circleForm(2);
                            echo circleForm(3);
                        } elseif ($shape1 == "rectangle" && $shape2 == "rectangle" && $shape3 == "rectangle"){
                            echo rectangleForm(1);
                            echo rectangleForm(2);
                            echo rectangleForm(3);                            
                        } elseif ($shape1 == "rectangle" && $shape2 == "circle" && $shape2 == "circle") {
                            echo rectangleForm();
                            echo circleForm(1);
                            echo circleForm(2);                                   
                        } elseif ($shape1 == "circle" && $shape2 == "rectangle"&& $shape3 == "rectangle") {
                            echo circleForm();
                            echo rectangleForm(1);
                            echo rectangleForm(2);
                        } elseif ($shape1 == "rectangle" && $shape2 =="circle" && $shape3 == "circle") {
                            
                        
                        } else  echo "<br>Warning: This function doesn't exist yet!<br><br>";

                        echo '<input type="submit" class="btn btn-primary" value="Check collisions">
                            </form>';
                        ?>

                    </div>
                    <div class="col"></div>
                </div>
            </div>
        </section>
    </div>
</main>