<?php require_once('header.php'); ?>

<main>
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h3 class="text-info">Insert Shapes</h3>
                    <form action="getData.php" method="POST">
                        <label class='col-sm-4'><b>Shape1:</b>
                            <input name='shape1' class='form-control' value="<?php if (isset($_GET['shape1'])) echo $_GET['shape1']; ?>" type='text'>
                        </label>
                        <label class='col-sm-4'><b>Shape2</b>:
                            <input name='shape2' class='form-control' type='text' value="<?php if (isset($_GET['shape2'])) echo $_GET['shape2'];?>">
                        </label>
                        <hr>
                        <button class=" btn btn-success">Submit shapes</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>

