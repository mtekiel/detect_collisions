<?php
require_once('functions.php');
require_once('header.php');
?>
<main>
    <div class="content-wrapper">
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-3"><a href="index.php" class="btn btn-warning">Home</a></div>
                    <div class="col-6">
                        <?php
                        $shape1 = $_POST['shape1'];
                        $shape2 = $_POST['shape2'];

                        //Check what kind of collision just happens and call appropriate function 
                        if ($shape1 == "point" && $shape2 == "point") {

                            $x1 = $_POST['px1'];
                            $y1 = $_POST['py1'];
                            $x2 = $_POST['px2'];
                            $y2 = $_POST['py2'];

                            //Validation of input data       
                            if (!(is_numeric($x1) && is_numeric($y1) && is_numeric($x2) && is_numeric($y2))) {
                                echo "No full data set or incorrect data";
                                return false;
                            };
                            pointPoint($x1, $y1, $x2, $y2);
                        } elseif (($shape1 == "point" && $shape2 == "circle") || ($shape1 == "circle" && $shape2 == "point")) {
                            $px = $_POST['px'];
                            $py = $_POST['py'];
                            $circleX = $_POST['circleX'];
                            $circleY = $_POST['circleY'];
                            $radius = $_POST['radius'];

                            pointCircle($px, $py, $circleX, $circleY, $radius);
                        } elseif ($shape1 == "circle" && $shape2 == "circle") {

                            $circle1X = $_POST['circle1X'];
                            $circle1Y = $_POST['circle1Y'];
                            $radius1 = $_POST['radius1'];

                            $circle2X = $_POST['circle2X'];
                            $circle2Y = $_POST['circle2Y'];
                            $radius2 = $_POST['radius2'];

                            if (!(is_numeric($circle1X) && is_numeric($circle1Y) && is_numeric($radius1) && is_numeric($circle2X) && is_numeric($circle2Y) && is_numeric($radius2))) {
                                echo "No full data set or incorrect data";
                                return false;
                            };

                            circleCircle($circle1X, $circle1Y, $radius1, $circle2X, $circle2Y, $radius2);
                        } elseif (($shape1 == "point" && $shape2 == "rectangle") || ($shape1 == "rectangle" && $shape2 == "point")) {

                            $px = $_POST['px'];
                            $py = $_POST['py'];
                            $rectLBCornerX = $_POST['rectLBCornerX'];
                            $rectLBCornerY = $_POST['rectLBCornerY'];
                            $rectWidth = $_POST['rectWidth'];
                            $rectHeight = $_POST['rectHeight'];

                            if (!(is_numeric($px) && is_numeric($py) && is_numeric($rectLBCornerX) && is_numeric($rectLBCornerY) && is_numeric($rectWidth) && is_numeric($rectHeight))) {
                                echo "No full data set or incorrect data";
                                return false;
                            };

                            pointRect($px,  $py,  $rectLBCornerX,  $rectLBCornerY,  $rectWidth,  $rectHeight);
                        } elseif ($shape1 == "rectangle" && $shape2 == "rectangle") {

                            $rect1LBCornerX = $_POST['rect1LBCornerX'];
                            $rect1LBCornerY = $_POST['rect1LBCornerY'];
                            $rect1Width = $_POST['rect1Width'];
                            $rect1Height = $_POST['rect1Height'];
                            $rect2LBCornerX = $_POST['rect2LBCornerX'];
                            $rect2LBCornerY = $_POST['rect2LBCornerY'];
                            $rect2Width = $_POST['rect2Width'];
                            $rect2Height = $_POST['rect2Height'];

                            if (!(is_numeric($rect1LBCornerX) &&  is_numeric($rect1LBCornerY) && is_numeric($rect1Width) && is_numeric($rect1Height) && is_numeric($rect2LBCornerX) && is_numeric($rect2LBCornerY) && is_numeric($rect2Width) && is_numeric($rect2Height))) {
                                echo "No full data set or incorrect data";
                                return false;
                            };

                            rectRect($rect1LBCornerX,  $rect1LBCornerY, $rect1Width, $rect1Height, $rect2LBCornerX, $rect2LBCornerY, $rect2Width, $rect2Height);
                        } elseif (($shape1 == "circle" && $shape2 == "rectangle") || ($shape1 == "rectangle" && $shape2 == "circle")) {

                            $circleX = $_POST['circleX'];
                            $circleY = $_POST['circleY'];
                            $radius = $_POST['radius'];
                            $rectLBCornerX = $_POST['rectLBCornerX'];
                            $rectLBCornerY = $_POST['rectLBCornerY'];
                            $rectWidth = $_POST['rectWidth'];
                            $rectHeight = $_POST['rectHeight'];

                            if (!(is_numeric($circleX) && is_numeric($circleY) && is_numeric($radius) && is_numeric($rectLBCornerX) && is_numeric($rectLBCornerY) && is_numeric($rectWidth) && is_numeric($rectHeight))) {
                                echo "No full data set or incorrect data";
                                return false;
                            };

                            circleRect($circleX, $circleY, $radius, $rectLBCornerX, $rectLBCornerY, $rectWidth, $rectHeight);
                            
                        } elseif (($shape1 == "triangle" && $shape2 == "point") || ($shape1 == "point" && $shape2 == "triangle")) {

                            $triangleX1 = $_POST['triangleX1'];
                            $triangleY1 = $_POST['triangleY1'];
                            $triangleX2 = $_POST['triangleX2'];
                            $triangleY2 = $_POST['triangleY2'];
                            $triangleX3 = $_POST['triangleX3'];
                            $triangleY3 = $_POST['triangleY3'];
                            $px = $_POST['px'];
                            $py = $_POST['py'];


                            if (!(is_numeric($triangleX1) && is_numeric($triangleY1) && is_numeric($triangleX2) && is_numeric($triangleY2) && is_numeric($triangleX3) && is_numeric($triangleY3) && is_numeric($px) && is_numeric($py))) {
                                echo "No full data set or incorrect data";
                                return false;
                            };

                            trianglePoint($triangleX1, $triangleY1, $triangleX2, $triangleY2, $triangleX3, $triangleY3, $px, $py);
                        };
                        ?>

                    </div>
                    <div class="col">

                    </div>

                    <div class="row">
                        <div class="col-10">
                            <canvas id="myCanvas" width="1200" height="550" style="border:1px solid #d3d3d3; background-color: #F5F5DC;">
                            </canvas>
                            <script>
                                var c = document.getElementById("myCanvas");
                                var ctx = c.getContext("2d");
                                var graduation = 1;
                                var centerX = 600;
                                var centerY = 250;

                                function drawBoard() {
                                    ctx.beginPath();
                                    ctx.font = "10px Arial";

                                    ctx.fillText("50", 650, 260);
                                    ctx.fillText("100", 700, 260);
                                    ctx.fillText("50", 585, 210);
                                    ctx.fillText("100", 583, 160);

                                    ctx.moveTo(0, 250);
                                    ctx.lineTo(1200, 250);
                                    ctx.moveTo(600, 0);
                                    ctx.lineTo(600, 550);
                                    ctx.stroke();
                                }


                                function draw2Circles() {
                                    <?php

                                    echo "var circle1X ='$circle1X';";
                                    echo "var circle1Y ='$circle1Y';";
                                    echo "var radius1 ='$radius1';";

                                    echo "var circle2X ='$circle2X';";
                                    echo "var circle2Y ='$circle2Y';";
                                    echo "var radius2 ='$radius2';";
                                    ?>
                                    ctx.beginPath();
                                    ctx.strokeStyle = "#FF0000";
                                    ctx.arc(circle1X - (-centerX), centerY - circle1Y, radius1 * graduation, 0, 2 * Math.PI);

                                    ctx.stroke();
                                    ctx.beginPath();
                                    ctx.strokeStyle = "	#228B22";
                                    ctx.arc(circle2X - (-centerX), centerY - circle2Y, radius2 * graduation, 0, 2 * Math.PI);
                                    ctx.stroke();
                                }

                                drawBoard();
                                draw2Circles();
                            </script>


                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>