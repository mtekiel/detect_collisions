<?php

use PHPUnit\Framework\TestCase;
include('../functions.php');

class CollisionFuntionsTest extends TestCase{
    
    //Two points collision detection
    public function test_collision_of_two_points_with_same_coordinates_should_return_true(){
        $collision = pointPoint(1,1,1,1);
        $this->assertEquals($collision, true);
    }
    public function test_points_collision_with_different_coordinates_should_return_false(){
        $collision = pointPoint(1,2,3,5);
        $this->assertEquals($collision, false);
    }

    //Test point-cirlce collision detection
    public function test_detect_point_circle_collision_should_return_true(){
        $collision = pointCircle(1, 1, 2, 2, 1.5);
        $this->assertEquals($collision, true);
    }

    public function test_detect_point_circle_collision_should_return_false()
    {
        $collision = pointCircle(1, 1, 2, 2, 1.1);
        $this->assertEquals($collision, false);
    }

    //Test circle-cirlce collision detection
    public function test_detect_cirlce_circle_collision_should_return_true()
    {
        $collision = circleCircle(1, 1, 1, 3, 3, 2);
        $this->assertEquals($collision, true);
    }

    public function test_detect_cirlce_circle_collision_should_return_false()
    {
        $collision = circleCircle(1, 1, 1, 3, 3, 1 );
        $this->assertEquals($collision, false);
    }

    //Test point-rectangle collision detection
    public function test_detect_point_rectangle_collision_should_return_true()
    {
        $collision = pointRect(1, 1, 0, 0, 3, 2);
        $this->assertEquals($collision, true);
    }

    public function test_detect_point_rectangle_collision_should_return_false()
    {
        $collision = pointRect(1, 1, 2, 2, 3, 1);
        $this->assertEquals($collision, false);
    }

    //Test rectangle-rectangle collision detection
    public function test_detect_rectangle_rectangle_collision_should_return_true()
    {
        $collision = rectRect(1, 1, 1, 1, 2, 2, 3, 2);
        $this->assertEquals($collision, true);
    }

    public function test_detect_rectangle_rectangle_collision_should_return_false()
    {
        $collision = rectRect(1, 1, 1, 1, 2.5, 2.5, 3, 2);
        $this->assertEquals($collision, false);
    }

    //Test circle-rectangle collision detection
    public function test_detect_circle_rectangle_collision_should_return_true()
    {
        $collision = circleRect(1, 1, 1, 1, 1, 2, 3);
        $this->assertEquals($collision, true);
    }

    public function test_detect_circle_rectangle_collision_should_return_false()
    {
        $collision = circleRect(1, 1, 1, 2, 2, 3, 2);
        $this->assertEquals($collision, false);
    }

    //Test triangle-point collision detection
    public function test_detect_triangle_point_collision_should_return_true()
    {
        $collision = trianglePoint(-1, -1, 1, 3, 3, 1, -1, -1);
        $this->assertEquals($collision, true);
    }
    
    public function test_detect_triangle_point_collision_should_return_false()
    {
        $collision = trianglePoint(1, 1, 1, 3, 3, 1, 2.5, 2.5);
        $this->assertEquals($collision, false);
    }

}