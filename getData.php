<?php
require_once('functions.php');
require_once('header.php');
?>
<main>
    <div class="content-wrapper">
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-3"><a href="index.php" class="btn btn-warning">Back</a></div>
                    <div class="col-6">
                        <?php
                        $shape1 = $_POST['shape1'];
                        $shape2 = $_POST['shape2'];

                        if (!$shape1 or !$shape2) {
                            echo "Input shape";
                        }

                        //Repetitive part of form
                        echo '<form action="checkCollisions.php" method="POST">
                                <b><label>Object1</label>
                                <input type="text" name="shape1" value="' . $shape1 . '">&nbsp&nbsp                                     
                                <label>Object2</label>
                                <input type="text" name="shape2" value="' . $shape2 . '"><br><br></b>';

                        //Check what kind of collision we just have and load appropriate form for data
                        if ($shape1 == "point" && $shape2 == "point") {
                            echo '<b><span>Point1</span></b><br>
                                <label>Point1 X coordinate</label>
                                <input type="text" name="px1"><br>
                                <label>Point1 Y coordinate</label>
                                <input type="text" name="py1"><br><br>

                                <b><span>Point2</span></b><br>
                                <label>Point2 X coordinate</label>
                                <input type="text" name="px2"><br>
                                <label>Point2 Y coordinate</label>
                                <input type="text" name="py2"><br><br>';

                        } elseif (($shape1 == "point" && $shape2 == "circle") || ($shape1 == "circle" && $shape2 == "point")) {
                            echo pointForm();
                            echo circleForm(); 
                                    
                        } elseif ($shape1 == "circle" && $shape2 == "circle"){
                            echo circleForm(1);
                            echo circleForm(2);
                                
                        }elseif (($shape1 == "point" && $shape2 == "rectangle") || ($shape1 == "rectangle" && $shape2 == "point")){
                           
                            echo pointForm();
                            echo rectangleForm();
                            
                        }elseif ($shape1 == "rectangle" && $shape2 == "rectangle") {
                            echo rectangleForm(1);
                            echo rectangleForm(2);

                        }elseif (($shape1 == "circle" && $shape2 == "rectangle") || ($shape1 == "rectangle" && $shape2 == "circle")){
                            echo circleForm();
                            echo rectangleForm();
                            
                        } elseif ((($shape1 == "point" && $shape2 == "triangle") || ($shape1 == "triangle" && $shape2 == "point"))) {
                            echo '<b><span>Triangle</span></b><br>
                                <label>Triangle vertex1 X</label>
                                <input type="text" name="triangleX1"><br>
                                <label>Triangle vertex1 Y</label>
                                <input type="text" name="triangleY1"><br><br>

                                <label>Triangle vertex2 X</label>
                                <input type="text" name="triangleX2"><br>
                                <label>Triangle vertex2 Y</label>
                                <input type="text" name="triangleY2"><br><br>

                                <label>Triangle vertex3 X</label>
                                <input type="text" name="triangleX3"><br>
                                <label>Triangle vertex3 Y</label>
                                <input type="text" name="triangleY3"><br><br>';

                            echo pointForm();   

                        } else  echo "<br>Warning: This function doesn't exist yet!<br><br>";
                        
                            echo '<input type="submit" class="btn btn-primary" value="Check collisions">
                        
                            </form>';
                        ?>
                        
                    </div>
                    <div class="col"></div>
                </div>
            </div>
        </section>
    </div>
</main>