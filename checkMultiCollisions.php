<?php
require_once('functions.php');
require_once('header.php');
?>
<main>
    <div class="content-wrapper">
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-3"><a href="multiObjects.php" class="btn btn-warning">Home</a></div>
                    <div class="col-6">
                        <?php
                        $shape1 = $_POST['shape1'];
                        $shape2 = $_POST['shape2'];
                        $shape3 = $_POST['shape3'];

                        //Check what kind of collisions just happens and call appropriate functions 
                        if ($shape1 == "circle" && $shape2 == "circle" && $shape3 == "circle" ) {
                            //We have 3 circles so we need to check 3 cases
                            // -  collision between circle1 and circle2 
                            // -     ...            circle2 and circle3
                            // -     ...            circle1 and circle3

                            $circle1X = $_POST['circle1X'];
                            $circle1Y = $_POST['circle1Y'];
                            $radius1 = $_POST['radius1'];

                            $circle2X = $_POST['circle2X'];
                            $circle2Y = $_POST['circle2Y'];
                            $radius2 = $_POST['radius2'];

                            $circle3X = $_POST['circle3X'];
                            $circle3Y = $_POST['circle3Y'];
                            $radius3 = $_POST['radius3'];

                            //Validation of input data       
                            if (!(is_numeric($circle1X) && is_numeric($circle1Y) && is_numeric($radius1) 
                                && is_numeric($circle2X) && is_numeric($circle2Y) && is_numeric($radius2) &&is_numeric($circle3X)
                                && is_numeric($circle3Y) && is_numeric($radius3))) {

                                echo "No full data set or incorrect data";
                                return false;
                            };
                           
                            circleCircle($circle1X, $circle1Y, $radius1, $circle2X, $circle2Y, $radius2);                           
                            circleCircle($circle2X, $circle2Y, $radius2, $circle3X, $circle3Y, $radius3);                            
                            circleCircle($circle1X, $circle1Y, $radius1, $circle3X, $circle3Y, $radius3);

                        } elseif ($shape1 == "rectangle" && $shape2 == "rectangle" && $shape3 == "rectangle") {
                            $rect1LBCornerX = $_POST['rect1LBCornerX'];
                            $rect1LBCornerY = $_POST['rect1LBCornerY'];
                            $rect1Width = $_POST['rect1Width'];
                            $rect1Height = $_POST['rect1Height'];

                            $rect2LBCornerX = $_POST['rect2LBCornerX'];
                            $rect2LBCornerY = $_POST['rect2LBCornerY'];
                            $rect2Width = $_POST['rect2Width'];
                            $rect2Height = $_POST['rect2Height'];

                            $rect3LBCornerX = $_POST['rect3LBCornerX'];
                            $rect3LBCornerY = $_POST['rect3LBCornerY'];
                            $rect3Width = $_POST['rect3Width'];
                            $rect3Height = $_POST['rect3Height'];

                            if (!(is_numeric($rect1LBCornerX) && is_numeric($rect1LBCornerY) &&
                            $rect1Width && $rect1Height && is_numeric($rect2LBCornerX) && is_numeric($rect2LBCornerY) &&
                            $rect2Width && $rect2Height && is_numeric($rect3LBCornerX) && is_numeric($rect3LBCornerY) &&
                            $rect3Width && $rect3Height)) {
                                echo "No full data set or incorrect data";
                                return false;
                            };    

                            rectRect($rect1LBCornerX,  $rect1LBCornerY, $rect1Width, $rect1Height, $rect2LBCornerX, $rect2LBCornerY, $rect2Width, $rect2Height);
                            rectRect($rect1LBCornerX,  $rect1LBCornerY, $rect1Width, $rect1Height, $rect3LBCornerX, $rect3LBCornerY, $rect3Width, $rect3Height);
                            rectRect($rect3LBCornerX,  $rect3LBCornerY, $rect3Width, $rect3Height, $rect2LBCornerX, $rect2LBCornerY, $rect2Width, $rect2Height);   
                                                        
                        } elseif ($shape1 == "rectangle" && $shape2 == "circle" && $shape3 == "circle") {

                            $rectLBCornerX = $_POST['rectLBCornerX'];
                            $rectLBCornerY = $_POST['rectLBCornerY'];
                            $rectWidth = $_POST['rectWidth'];
                            $rectHeight = $_POST['rectHeight'];
                            
                            $circle1X = $_POST['circle1X'];
                            $circle1Y = $_POST['circle1Y'];
                            $radius1 = $_POST['radius1'];

                            $circle2X = $_POST['circle2X'];
                            $circle2Y = $_POST['circle2Y'];
                            $radius2 = $_POST['radius2'];

                            if (!(is_numeric($circle1X) && is_numeric($circle1Y) && is_numeric($radius1) && is_numeric($circle2X) && is_numeric($circle2Y) && is_numeric($radius2)&& is_numeric($rectLBCornerX) && is_numeric($rectLBCornerY) &&
                            $rectWidth && $rectHeight)) {
                                echo "No full data set or incorrect data";
                                return false;
                            };

                            //We must check cases - Rect-Circle1, Rect-Circle2 and Circle1-Circle2  

                            circleRect($circle1X, $circle1Y, $radius1, $rectLBCornerX, $rectLBCornerY, $rectWidth, $rectHeight);
                            circleRect($circle2X, $circle2Y, $radius2, $rectLBCornerX, $rectLBCornerY, $rectWidth, $rectHeight);
                            circleCircle($circle1X, $circle1Y, $radius1, $circle2X, $circle2Y, $radius2);

                        } elseif ($shape1 == "circle" && $shape2 == "rectangle"&& $shape3 == "rectangle"){

                            $circleX = $_POST['circleX'];
                            $circleY = $_POST['circleY'];
                            $radius = $_POST['radius'];

                            $rect1LBCornerX = $_POST['rect1LBCornerX'];
                            $rect1LBCornerY = $_POST['rect1LBCornerY'];
                            $rect1Width = $_POST['rect1Width'];
                            $rect1Height = $_POST['rect1Height'];

                            $rect2LBCornerX = $_POST['rect2LBCornerX'];
                            $rect2LBCornerY = $_POST['rect2LBCornerY'];
                            $rect2Width = $_POST['rect2Width'];
                            $rect2Height = $_POST['rect2Height'];

                            if (!(is_numeric($circleX) && is_numeric($circleY) && is_numeric($radius) && is_numeric($rect1LBCornerX) && is_numeric($rect1LBCornerY) &&
                            $rect1Width && $rect1Height && is_numeric($rect2LBCornerX) && is_numeric($rect2LBCornerY) &&
                            $rect2Width && $rect2Height)) {
                                echo "No full data set or incorrect data";
                                return false;
                            };       

                            //We must check cases - Rect1-Circle, Rect2-Circle and Rect1-Rect2
                            circleRect($circleX, $circleY, $radius, $rect1LBCornerX, $rect1LBCornerY, $rect1Width, $rect1Height);                       
                            circleRect($circleX, $circleY, $radius, $rect2LBCornerX, $rect2LBCornerY, $rect2Width, $rect2Height);
                            rectRect($rect1LBCornerX,  $rect1LBCornerY, $rect1Width, $rect1Height, $rect2LBCornerX, $rect2LBCornerY, $rect2Width, $rect2Height);                      
                        };      
                        ?>

                    </div>
                    <div class="col">
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>